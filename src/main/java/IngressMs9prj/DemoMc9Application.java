package IngressMs9prj;

import IngressMs9prj.model.Address;
import IngressMs9prj.model.Contact;
import IngressMs9prj.model.User;
import IngressMs9prj.model.UserRole;
import IngressMs9prj.repository.AddressRepository;
import IngressMs9prj.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import javax.transaction.Transactional;
import java.util.Set;

@SpringBootApplication
@EnableConfigurationProperties
@RequiredArgsConstructor
public class DemoMc9Application implements CommandLineRunner {

    private final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoMc9Application.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception{

        Contact contact1= Contact.builder().contactNumber("6787").build();
        Contact contact2= Contact.builder().contactNumber("5456").build();
        Set<Contact> contacts=Set.of(contact1,contact2);

        Address address= Address.builder().name("Babek proc.").contacts(contacts).build();

        UserRole userRole1 =new UserRole();
        userRole1.setRole("admin");
        UserRole userRole2 =new UserRole();
        userRole2.setRole("owner");
        Set<UserRole> userRoles =Set.of(userRole1,userRole2);


        User user=new User();
        user.setUserRoles(userRoles);
        user.setUserName("Aysun");
        user.setAddress(address);
        user.setPassword("87546");


        userRepository.save(user);

    }
}
