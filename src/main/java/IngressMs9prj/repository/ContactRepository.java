package IngressMs9prj.repository;

import IngressMs9prj.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Address,Long> {

}
